# Welcome to Clay

This is a small news feed site powered by a CMS, built using ProcessWire. It serves as a display for the imaginary organization Clay.

## Table of Contents
1. [About ProcessWire](#about-processwire)



## About ProcessWire

ProcessWire is an open source content management system (CMS) and web
application framework aimed at the needs of designers, developers and their
clients. ProcessWire gives you more control over your fields, templates and
markup than other platforms, and provides a powerful template system that
works the way you do. Not to mention, ProcessWire's API makes working with
your content easy and enjoyable. Managing and developing a site in
ProcessWire is shockingly simple compared to what you may be used to.

* [ProcessWire Home](https://processwire.com)
* [API Reference](https://processwire.com/api/ref/)
* [Download](https://processwire.com/download/)
* [Support](https://processwire.com/talk/)
* [Modules/Plugins](http://modules.processwire.com)
