

	</div><!--/#main-->

	<!-- footer -->
	<footer id='footer'>
		<div class="container">
			<div class="row">

				<p>
					Powered by <a href='http://processwire.com'>ProcessWire CMS</a>  &nbsp; / &nbsp;
					<?php
					if($user->isLoggedin()) {
						// if user is logged in, show a logout link
						echo "<a href='{$config->urls->admin}login/logout/'>Logout ($user->name)</a>";
					} else {
						// if user not logged in, show a login link
						echo "<a href='{$config->urls->admin}'>Admin Login</a>";
					}
					?>
				</p>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="<?php echo $config->urls->templates; ?>scripts/bootstrap.min.js">
	</script>
	<script type="text/javascript" src="<?php echo $config->urls->templates; ?>scripts/main.js">
	</script>

</body>
</html>
