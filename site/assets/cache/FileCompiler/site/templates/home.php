<?php

 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . 'site/templates/_head.php',array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); // include header markup ?>

	<section id='content' class='container'>
		<div class='row'>
			<div class='col-xs-12'>

				<h1>Welcome to Clay's home on the web!</h1>
				<h2>Keep up with the latest news <a href="news">here</a>!</h2>

			</div>
		</div>
	</section><!-- end content -->

<?php include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . 'site/templates/_foot.php',array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); // include footer markup ?>
