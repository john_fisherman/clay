<?php

include('./_head.php'); // include header markup ?>

<section id='content' class='container news-page'>
  <div class='row news-headline'>
    <div class='col-xs-12 col-sm-4'>
          <?php
            // output 'headline' if available, otherwise 'title'
            echo "<h1>" . $page->get('headline|title') . "</h1>";

            // output bodycopy
            // echo $page->body;
          ?>
    </div>
  </div>
  <?php

    // Iterate all children, output image, title and trimmed body
    $news = $page->children();

    foreach($news as $newsItem) {

  ?>

  <div class='row'>
    <div class='col-xs-12 col-sm-4'>
      <?php
        $newsImage = $newsItem->images->first();

        // $newsImage and $thumb are both Pageimage objects
        // $thumb = $newsImage->size(200, 200);
        echo "<a href='{$newsItem->url}'>";
        echo "<img src='$newsImage->url' alt='$newsImage->description' />";
        echo "</a>";
      ?>
    </div>
    <div class='col-xs-12 col-sm-8'>
      <?php
        echo "<h2><a href='{$newsItem->url}'>{$newsItem->title}</a></h2>";
        echo "<h3>By " . $newsItem->createdUser->name . ", on " . date("m/d/Y", $newsItem->created) . "</h3>";
        echo "<p>" . mb_strimwidth($newsItem->body, 0, 300, "...") . "</p>";
      ?>
      <a href="<?php echo $newsItem->url; ?>"><button class="readmore-button btn btn-action">Read More</button></a>
    </div>

  </div><!-- end row (news item) -->

  <?php } // end foreach ?>

</section><!-- end content -->

<?php include('./_foot.php'); // include footer markup ?>
