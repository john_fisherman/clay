<?php

include('./_head.php'); // include header markup ?>

<section id='content' class='container news-item'>
		<div class='row'>
    	<div class='col-xs-12'>
			  <div class="news-item-image">
					<?php
				    if(count($page->images)) {
				      foreach($page->images as $image) {

				        // crop image and make it more panoramic
								$cropped_image = $image->crop(0, 200, 1170, 300);
				        // output the image ...
				        echo "<img src='$cropped_image->url' alt='$cropped_image->description' />";
				      }
				    }
			  	?>
				</div><!-- end image -->
				<div>
				<?php
					// output 'headline' if available, otherwise 'title'
					echo "<h1>" . $page->get('title') . "</h1>";

					echo "<h2>By " . $page->createdUser->name . ", on " . date("m/d/Y", $page->created) . "</h2>";
					// output bodycopy
					echo $page->body;
				?>
			</div>
		</div>
	</div>
</section>


<?php include('./_foot.php'); // include footer markup ?>
