<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php echo $page->title; ?></title>
	<meta name="description" content="<?php echo $page->summary; ?>" />
	<link href='//fonts.googleapis.com/css?family=Lato:400,400i,700|Quattrocento:400,700' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" />
	<script
	  src="https://code.jquery.com/jquery-3.2.1.min.js"
	  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	  crossorigin="anonymous">
	</script>
</head>
<body>
	<section id='top-header'>
		<div class='container'>
			<div class='row'>
				<div class='col-xs-12'>

					<nav class="navbar navbar-default">
					  <div class="container-fluid">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <a class="navbar-brand" href="<?php echo $pages->get('/')->url; ?>"><img id="logo" src="<?php echo $config->urls->templates?>img/logo.png" alt=""></a>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav">

									<?php

										// top navigation consists of homepage and its visible children
										$homepage = $pages->get('/');
										$children = $homepage->children();

										// make 'home' the first item in the navigation
										$children->prepend($homepage);

										// render an <li> for each top navigation item
										foreach($children as $child) {
											if($child->id == $page->rootParent->id) {
												// this $child page is currently being viewed (or one of it's children/descendents)
												// so we highlight it as the current page in the navigation
												echo "<li class='current'><a href='$child->url'>$child->title</a></li>";
											} else {
												echo "<li><a href='$child->url'>$child->title</a></li>";
											}
										}

										// output an "Edit" link if this page happens to be editable by the current user
										if($page->editable()) {
											echo "<li class='edit'><a href='$page->editUrl'>Edit</a></li>";
										}

									?>

					      </ul>
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>

					<!-- top navigation -->
					<ul class='topnav'></ul>
				</div>
			</div>
		</div>
	</section> <!-- end top header -->

	<!-- search form -->
	<!-- <form class='search' action='<?php echo $pages->get('template=search')->url; ?>' method='get'>
		<input type='text' name='q' placeholder='Search' value='' />
		<button type='submit' name='submit'>Search</button>
	</form> -->

	<!-- breadcrumbs -->
	<div class='breadcrumbs'><?php

		// // breadcrumbs are the current page's parents
		// foreach($page->parents() as $item) {
		// 	echo "<span><a href='$item->url'>$item->title</a></span> ";
		// }
		// // optionally output the current page as the last item
		// echo "<span>$page->title</span> ";

	?></div>

	<div id='main'>
