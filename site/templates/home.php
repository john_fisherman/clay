<?php

include('./_head.php'); // include header markup ?>

	<section id='content' class='container'>
		<div class='row'>
			<div class='col-xs-12'>

				<h1>Welcome to Clay's home on the web!</h1>
				<h2>Keep up with the latest news <a href="news">here</a>!</h2>

			</div>
		</div>
	</section><!-- end content -->

<?php include('./_foot.php'); // include footer markup ?>
